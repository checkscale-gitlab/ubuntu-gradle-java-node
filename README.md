# ubuntu-gradle-java-node <small>(_gregswindle/ubuntu-gradle-java-node_)</small>

[![docker build icon](https://assets.gitlab-static.net/uploads/-/system/project/avatar/18152254/icons8-docker-100.png)](https://gitlab.com/data-leakage-protection/ci-cd/ubuntu-gradle-java-node/#readme)

[![Docker Automated build](https://img.shields.io/docker/automated/gregswindle/ubuntu-gradle-java-node.svg)](https://hub.docker.com/r/gregswindle/ubuntu-gradle-java-node/) [![Docker Pulls](https://img.shields.io/docker/pulls/gregswindle/ubuntu-gradle-java-node.svg)](https://hub.docker.com/r/gregswindle/ubuntu-gradle-java-node/) [![Docker Stars](https://img.shields.io/docker/stars/gregswindle/ubuntu-gradle-java-node.svg)](https://hub.docker.com/r/gregswindle/ubuntu-gradle-java-node/)

> Docker image based on Ubuntu 18.04 with Java, Gradle and Node.

## Overview

This Docker image installs the following software on Ubuntu 18.04:

- [Gradle v6.3][gradle-releases]

- [Node.js v12.16.2][nodejs-releases]

- [openjdk-8-jre][openjdk-8-jre-intall]

## Usage

Reference this image in your <samp>.gitlab-ci.yml</samp> file, e.g.:

```dockerfile
semantic-release:
  image: gregswindle/ubuntu-gradle-java-node
```

## Maintainers

[Greg Swindle (@gregswindle)](https://github.com/gregswindle)

## Contributions

_We gratefully accept Merge Requests! Here's what you need to know to get started._

1.  Read our [Code of Conduct][code-of-conduct].

1.  Open an issue to Gitlab to

    - [report a **defect**][defect-report] or

    - [request a **feature**][feature-request]

You may also fork this repository and submit a [Merge Request][merge-request-new] when ready.

[![conventional commits](https://img.shields.io/badge/conventional%20commits-1.0.0-yellow.svg?style=flat-square)](https://www.conventionalcommits.org/en/v1.0.0-beta.2/#specification)

## License

 [![MIT License](https://img.shields.io/badge/license-MIT-blue.svg?style=flat-square)][license] © 2020 [Greg Swindle](https://gitlab.com/gregswindle).

## Attributions

This image is based on a fork of [karl-run/docker-java-node-gradle][attribution-fork].

[attribution-fork]: https://github.com/karl-run/docker-java-node-gradle#readme

[code-of-conduct]: https://gitlab.com/data-leakage-protection/ci-cd/ubuntu-gradle-java-node/-/blob/master/CODE_OF_CONDUCT.md#contributor-covenant-code-of-conduct

[defect-report]: https://gitlab.com/data-leakage-protection/ci-cd/ubuntu-gradle-java-node/issues/new?issuable_template=Defect&issue%5Btitle%5D=defect%28scope%29%3A+REPLACE_WITH_SUMMARY&labels=priority%3a+medium%2cstatus%3a+review+needed%2ctype%3a+defect&template=defect-report.md

[feature-request]: https://gitlab.com/data-leakage-protection/ci-cd/ubuntu-gradle-java-node/issues/new?issuable_template=Feature&issue%5Btitle%5D=feat%28scope%29%3A+REPLACE_WITH_SUMMARY&labels=priority%3a+medium%2cstatus%3a+review+needed%2ctype%3a+feature

[gradle-releases]: https://gradle.org/releases/

[merge-request-new]: https://gitlab.com/data-leakage-protection/ci-cd/ubuntu-gradle-java-node/-/merge_requests/new

[license]: https://gitlab.com/data-leakage-protection/ci-cd/ubuntu-gradle-java-node/-/blob/master/LICENSE

[nodejs-releases]: https://nodejs.org/en/download/releases/

[openjdk-8-jre-intall]: https://openjdk.java.net/install/